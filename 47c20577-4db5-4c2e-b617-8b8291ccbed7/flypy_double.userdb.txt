# Rime user dictionary
#@/db_name	flypy_double
#@/db_type	userdb
#@/rime_version	1.7.3
#@/tick	92
#@/user_id	47c20577-4db5-4c2e-b617-8b8291ccbed7
bu dv 	不对	c=2 d=1.28166 t=92
bu vi dc 	不知道	c=2 d=1.35195 t=92
bu xk 	不行	c=2 d=1.74307 t=92
ce lt 	策略	c=1 d=0.763379 t=92
ci dm 	词典	c=1 d=0.697676 t=92
d 	的	c=4 d=3.40469 t=92
da yb 	打印	c=2 d=1.67473 t=92
dc ui 	倒是	c=1 d=0.677057 t=92
ds xi 	东西	c=2 d=1.69156 t=92
dz 	都	c=2 d=1.83244 t=92
fav 	发	c=1 d=0.729789 t=92
gd ld gd qu 	改来改去	c=1 d=0.65377 t=92
gj j 	干就	c=1 d=0.748264 t=92
gj jt 	感觉	c=4 d=2.77187 t=92
gjxx jtx 	感觉	c=1 d=0.744532 t=92
hc dm 	好点	c=2 d=1.94575 t=92
hd yz 	还有	c=2 d=1.63338 t=92
ig xn 	成效	c=1 d=0.67032 t=92
j 	就	c=3 d=2.70483 t=92
jq 	久	c=3 d=2.68272 t=92
jx 	加	c=2 d=1.5615 t=92
l 	了	c=4 d=3.22736 t=92
lc di 	老弟	c=0 d=0.070822 t=92
ld 	来	c=1 d=0.715338 t=92
ldc 	莱	c=1 d=0.70822 t=92
ldc l 	莱了	c=-1 d=0 t=92
levl 	了	c=0 d=0.070822 t=92
levl lc di 	了老弟	c=1 d=0.71177 t=92
me 	么	c=2 d=1.70004 t=92
mw yz 	没有	c=5 d=3.85742 t=92
n 	你	c=4 d=3.74136 t=92
na ge 	哪个	c=1 d=0.687289 t=92
na ge 	那个	c=1 d=0.690734 t=92
ne 	呢	c=2 d=1.76059 t=92
ni 	泥	c=2 d=1.89771 t=92
nirx 	你	c=4 d=3.55661 t=92
s 	三	c=1 d=0.865022 t=92
tds 	台	c=1 d=0.771052 t=92
u 	是	c=4 d=3.41454 t=92
uf me 	什么	c=3 d=2.28077 t=92
ui 	时	c=1 d=0.822835 t=92
ui bu ui 	是不是	c=2 d=1.91678 t=92
uo 	说	c=3 d=2.79932 t=92
uv jn 	睡觉	c=2 d=1.87883 t=92
ve ge 	这个	c=1 d=0.67368 t=92
vs 	中	c=1 d=0.767206 t=92
vs tds 	中台	c=1 d=0.774916 t=92
vu yc 	主要	c=2 d=1.70856 t=92
w 	我	c=2 d=1.84163 t=92
wj l 	完了	c=1 d=0.752014 t=92
xm zd 	现在	c=4 d=3.07467 t=92
y g ie 	一个车	c=1 d=0.786628 t=92
yc 	要	c=2 d=1.85086 t=92
ye 	也	c=1 d=0.66034 t=92
yi ge 	一个	c=2 d=1.58909 t=92
ys hu 	用户	c=3 d=2.29177 t=92
yus 	预	c=1 d=0.726149 t=92
yus fav 	预发	c=2 d=1.47057 t=92
yz 	有	c=1 d=0.680451 t=92
zf me 	怎么	c=2 d=1.62523 t=92
zi dm 	字典	c=2 d=1.60906 t=92
